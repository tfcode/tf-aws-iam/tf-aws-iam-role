# AWS IAM ROLE
This module is designed to create an AWS IAM role. Currently this module, through the use of a 'feature flag', can be used to create the following:

* An IAM role for a user/group to assume _into_
* An IAM role to _be_ assumed into
