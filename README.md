<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

# AWS IAM ROLE
This module is designed to create an AWS IAM role. Currently this module, through the use of a 'feature flag', can be used to create the following:

* An IAM role for a user/group to assume _into_
* An IAM role to _be_ assumed into

## Usage

```hcl
module "aws_iam_role" {
  source               = "git@gitlab.com:tfcode/tf-aws-iam-role.git?ref=v1.0.0"
  role_name            = "MyAmazingRole"
  description          = "My Terraform managed role"
  role_path            = "/TerraformAdminRoles/"
  policy_path          = "/TerraformAdminPolicies/"
  max_session_duration = 4500
  assuming_principal   = "root"
  
  policy_arn = [
    "arn:aws:iam::aws:policy/AmazonS3FullAccess",
    "arn:aws:iam::aws:policy/AWSIoT1ClickReadOnlyAccess"
  ]
  
  tags = {
    Costcenter = "security"
    Environment = "prod"
  }

}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_assume_role_policy"></a> [assume_role_policy](#input_assume_role_policy) | The assume role policy to apply if not using the generic sts:AssumeRole with a standard principal | `string` | `null` | no |
| <a name="input_assuming_principal"></a> [assuming_principal](#input_assuming_principal) | The name of the principal to associate with the assume role policy. | `string` | n/a | yes |
| <a name="input_name"></a> [name](#input_name) | Alternative to iam_role_name to ensure naming happens regardless of what it's called. | `string` | `""` | no |
| <a name="input_policy_arn"></a> [policy_arn](#input_policy_arn) | List of previously created or AWS managed policies ARNs to attach. This is exclusively used with the role_policies variable | `list(string)` | n/a | yes |
| <a name="input_policy_path"></a> [policy_path](#input_policy_path) | Path to store the policy under | `string` | `"/TerraformManagedPolicies/"` | no |
| <a name="input_role_description"></a> [role_description](#input_role_description) | Meaninful description to provide the role | `string` | `"Terraform Managed Role"` | no |
| <a name="input_role_duration"></a> [role_duration](#input_role_duration) | Length (in seconds) to keep role session authenticated. | `number` | `3600` | no |
| <a name="input_role_name"></a> [role_name](#input_role_name) | The name of the role. | `string` | n/a | yes |
| <a name="input_role_path"></a> [role_path](#input_role_path) | Path to store the role under. | `string` | `"/TerraformManagedRoles/"` | no |
| <a name="input_role_perms_boundary"></a> [role_perms_boundary](#input_role_perms_boundary) | IAM permissions boundary to apply if applicable | `string` | `""` | no |
| <a name="input_tags"></a> [tags](#input_tags) | Tags to apply to resource and components created | `map(any)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_iam_policy_arn"></a> [iam_policy_arn](#output_iam_policy_arn) | The ARN assigned by AWS to this policy. |
| <a name="output_iam_policy_description"></a> [iam_policy_description](#output_iam_policy_description) | The description of the policy. |
| <a name="output_iam_policy_name"></a> [iam_policy_name](#output_iam_policy_name) | The name of the policy. |
| <a name="output_iam_policy_policy-id"></a> [iam_policy_policy-id](#output_iam_policy_policy-id) | The policy's ID. |
| <a name="output_iam_policy_tags"></a> [iam_policy_tags](#output_iam_policy_tags) | A map of tags assigned to the resource, including those inherited from the provider default_tags configuration block. |
| <a name="output_iam_role_arn"></a> [iam_role_arn](#output_iam_role_arn) | Amazon Resource Name (ARN) specifying the role. |
| <a name="output_iam_role_id"></a> [iam_role_id](#output_iam_role_id) | Name of the role. |
| <a name="output_iam_role_name"></a> [iam_role_name](#output_iam_role_name) | Name of the role. |
| <a name="output_iam_role_tags"></a> [iam_role_tags](#output_iam_role_tags) | A map of tags assigned to the resource, including those inherited from the provider default_tags configuration block. |

 ## Contribution
  Branch off main, open a PR! All are welcome!

  When youre ready to create a new release, just do the following:
  1. `git describe --tags --abbrev=0`
  2. Increment your version +1
  3. `git tag v$VERSION && git push origin v$VERSION`

  This file `README.md` is maintained by [terraform-docs](https://terraform-docs.io/), changes to this file may be overwritten.

  Additionally, this module relies on the following tools to be installed prior to committing any changes:
  * [terraform](https://www.terraform.io/) for `terraform fmt`
  * [tfsec](https://github.com/aquasecurity/tfsec) for security validation
  * [terraform-docs](https://terraform-docs.io/) for document generation
  * [pre-commit](https://pre-commit.com/)

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
